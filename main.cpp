//
//  main.cpp
//  regex
//
//  Created by Xiaochuan Qin on 12/1/13.
//  Copyright (c) 2013 Xiaochuan Qin. All rights reserved.
//

#include <iostream>
#include "dfa.hpp"
using namespace std;

void f1(){ cout <<"A"<<endl;}
void f2(){ cout <<"ABB"<<endl;}
void f3(){ cout << "A*B+" << endl;}
void f4(){ cout << "Whitespace" << endl;}
void f5(){ cout << "Keyword If" << endl;}
void f6(){ cout << "Keyword Then" << endl;}
void f7(){ cout << "ID" << endl;}
void f8(){ cout << "Real Number" <<endl; }


int main(int argc, const char * argv[])
{
    //std::string reg("\"a^B\\^c\\t((dD|e)*\\*\\||\\.[^][a-z\\*?+\\t^T\\]+)**a*+((b))*?[-abc][abc-][-a-c-]?+(Xy|uvW)?()(xyz)(b)[dsf]");
    //std::string reg( "abc?|D(gh)*i|jkl[^][a-zABC]+(x|yz)");
    std::set<char> allchar;
    for( char c = 0x20; c!= 0x7f; ++c) {
        allchar.insert(c);
    }
    //std::string reg("begin(abc)*[0-9a-zA-Z]((\\?(x|yz))+[m-nO-T])?end");
    //std::string reg("[^][0-9a-zA-Z]");
    //std::string reg("(a|b)*abb#");
    //std::string reg("(a|abb|a*b+)#");
    std::string reg(
"a#|(abb)#|(a*b+)#|[\t \n\r]*#|(if)#|(then)#|([_a-zA-Z][_a-zA-Z0-9]*)#|((0|([1-9][0-9]*))?.[0-9]*(e[+-]?[1-9][0-9]*)?)#");

    std::cout << reg << std::endl;
    /*for (auto t : make_verbose(expand_character_set(tokenize(reg), allchar))){
        cout << t.value;
    }
    cout <<endl;
    for (auto t : make_verbose(expand_character_set(tokenize(reg), allchar))){
        cout <<( t.type == OPERAND ? 0 : 1);
    }
    cout << endl;

    for (auto t :make_rpn(make_verbose(expand_character_set(tokenize(reg), allchar)))){
        cout << t.value;
    }
    cout <<endl;
    for (auto t :make_rpn(make_verbose(expand_character_set(tokenize(reg), allchar)))){
        cout <<( t.type == OPERAND ? 0 : 1);
    }
    cout << endl;

    return 0;*/
    DfaFactory dfaFactory('#');
    DFA dfa = dfaFactory.make_dfa(reg, {f1, f2, f3, f4, f5, f6, f7, f8});

    for( size_t i = 0; i < dfa.transitionTable.size(); ++i) {
        DFA::State& s = dfa.transitionTable[i];
        cout << (s.acceptable ? '*' : ' ');
        cout << i <<" {";
for(auto p : s.positions) {
            cout << p <<" ";
        }
        cout << "}: ";
        for(char c = 0; c < DFA::kCharacterSetSize; ++c) {
            if(s.move[c] != -1) {
                cout << c <<"->" << s.move[c] << " ";
            }
        }
        cout << dfa.actionTable[i];
        cout << endl;
    }
    
    
    
    dfa.simulate("if ab then abb abc1 xyz1 xyz a b .137 0.324 1.430 4000.23 40.900 40.980e1 40.98e-1 4.98e+3 4.98e+5.5 ");

    /*

    auto tableData = tree.construct_transition_table_ex();
    auto transTable = tableData.first;
    auto statePos = tableData.second;

    for( size_t i = 0; i < transTable.size(); ++i){
        cout << i <<" - {";
        for(auto p : statePos[i])
            cout << p <<" ";
        cout <<"}: ";
        for( char c = 0; c < State::kCharacterSetSize; ++c){
            int j;
            if( (j = transTable[i].next_state_idx(c)) >= 0){
                cout << c <<"->"<<j<<", ";
            }
        }
        cout << endl;
    }*/

    return 0;
}

