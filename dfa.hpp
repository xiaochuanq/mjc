//
//  dfa.hpp
//  regex
//
//  Created by Xiaochuan Qin on 12/4/13.
//  Copyright (c) 2013 Xiaochuan Qin. All rights reserved.
//

#ifndef DFA_HPP
#define DFA_HPP
#include <cstring>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include "regex_syntax_tree.hpp"
#include <iostream>


struct DfaConstructData {
    DfaConstructData(): position(-1), nullable(false) { }
    int position;
    int nullable;
    std::vector<int> firstpos;
    std::vector<int> lastpos;
};


class DfaFactory;
class DFA {
    friend class DfaFactory;
public:
    static const unsigned int kCharacterSetSize = 128;
    struct State {
        State():acceptable(false) {
            memset(move, -1, sizeof(int) * kCharacterSetSize);
        }
        bool acceptable;
        int move[kCharacterSetSize];
        std::vector<int> positions;
    };
public:
    typedef void(* ActionFuncType)();
    void simulate(const char* p) {
        int i = 0;
        std::string token;
        std::vector<int> backlog(1, i); // at state 0
        while(*p){
            int next =transitionTable[i].move[*p];
            std::cout <<"state " << i << ", char " << *p <<", next is " << next <<std::endl;
            if(next > 0){
                backlog.push_back(next); // now at state "next"
                token.push_back(*p);
                i = next;
                ++p;
            }
            else{ // no way to jump at here
                while(i > 0 && !transitionTable[i].acceptable){ // go back until either no way to back, or found a good state
                    backlog.pop_back();
                    i = backlog.back(); // go back one state
                    token.pop_back();
                    --p;
                }
                if(i > 0){
                    std::cout << "token is " << token << " type: ";
                    actionTable[i]();
                    token.clear();
                    i = 0;
                }
                else{
                    // restart the state machine at next available char
                    ++p;
                }
                
            }
	    }
    };
public:
    std::vector<State> transitionTable;
    std::vector<ActionFuncType> actionTable;
};

class DfaFactory {
    typedef RegexSyntaxTreeNode<DfaConstructData>* SyntaxTreeNodePtr;
    typedef RegexSyntaxTreeNode<DfaConstructData> SyntaxTreeNode;
public:
    DfaFactory(char a = '#', char charSetBeg = 0x20, char charSetEnd = 0x7f) : augmentChar(a) {
        assert(charSetEnd >= charSetBeg);
        for( char c = charSetBeg; c!= charSetEnd; ++c) {
            charSet.insert(c);
        }
    }
    
    DFA make_dfa( std::string regexp, const std::vector<DFA::ActionFuncType>& actions, DFA::ActionFuncType defaultAction = nullptr)
    {
        positionChars.clear();
        followPositions.clear();
        std::vector<RegexToken> rpn = make_rpn(make_verbose(expand_character_set(tokenize(regexp), charSet)));
        std::unique_ptr<SyntaxTreeNode> root( construct_regex_syntax_tree<DfaConstructData>(rpn));
        std::cout << make_expression(root.get());
        std::cout << std::endl;
        label_tree_node_and_comput_data(root.get()); // initializes tree node data, and collect position's character value to positionChars
        std::unordered_map<int, DFA::ActionFuncType> actionLookup;
        size_t ai = 0;
        for(size_t i = 0; i <  positionChars.size(); ++i){
            if(positionChars[i] == augmentChar) {
                augmentCharPositions.insert(i);
                actionLookup[i] = actions[ai++];
            }
        }
        for(auto it = actionLookup.cbegin(); it != actionLookup.cend(); ++it){
            std::cout << (it->first) <<": ";
            (it->second)();
        }
        followPositions.resize(positionChars.size(), std::vector<int>());
        evaluate_followpos(root.get());
        std::vector<int> nearestAugmentCharPositions = evaluate_nearest_augpos();
        for(int i =0; i < positionChars.size(); ++i)
            std::cout << i <<"\t";
        std::cout << std::endl;
        for(auto c : positionChars)
            std::cout << c <<"\t";
        std::cout << std::endl;
        for(int i =0; i < followPositions.size(); ++i) {
            std::cout << i << " : ";
            for(auto p : followPositions[i])
                std::cout << p <<", ";
            std::cout << std::endl;
        }
        DFA dfa;
        dfa.transitionTable  = construct_transition_table(root.get());
        dfa.actionTable.resize(dfa.transitionTable.size(), defaultAction);
        auto& states = dfa.transitionTable;
        assert(states.size() > 0);
        for(size_t i = 0; i < states.size()-1; ++i) { //for each state
            for(char c = 0; c < DFA::kCharacterSetSize; ++c) {
                int next = states[i].move[c];
                if(next > 0 && dfa.actionTable[next] == nullptr) { //get an unregistered state "next"
                    for(int p : states[i].positions) { //loop all possible next step positions
                        if(positionChars[p] == c) {
                            int a  = nearestAugmentCharPositions[p];
                            if( a >= 0){
                // if the first found transition leads to a position just before augment char, i.e., concludes a state,
                                dfa.actionTable[next] = actionLookup[a]; // register that state's action
                                break;
                            }
                            
                        }
                    }
                }
            }
        }
        for(size_t i = 0; i < dfa.actionTable.size(); ++i){
            std::cout << "State " << i << " action: ";
            if( dfa.actionTable[i] )
                dfa.actionTable[i]();
            else
                std::cout << "NO OP"<<std::endl;
        }
        // clear_regex_syntax_tree_data(root);
        return dfa;
    }
    
private:
    char augmentChar;
    std::set<char> charSet;
    std::vector<std::vector<int> > followPositions;
    std::vector<char> positionChars;
    std::unordered_set<int> augmentCharPositions;
private:
    void label_tree_node_and_comput_data(SyntaxTreeNodePtr root)
    {
        if(!root)
            return;
        
        if(root->is_leaf()) { // root is operand
            if(root->token().value) { // operand is not epslion
                positionChars.push_back(root->token().value);
                root->data().position = static_cast<int>(positionChars.size()-1);
                root->data().nullable = false;
                root->data().firstpos.push_back(root->data().position);
                root->data().lastpos.push_back(root->data().position);
            }
            else {
                root->data().nullable = true;
            }
            return;
        }
        
        // else, root is operator
        if('*' == root->token().value ) {
            label_tree_node_and_comput_data(root->child());
            root->data().nullable = true;
            root->data().firstpos = root->child()->data().firstpos;
            root->data().lastpos = root->child()->data().lastpos;
        }
        else {
            label_tree_node_and_comput_data(root->left());
            label_tree_node_and_comput_data(root->right());
            if('|' == root->token().value) {
                root->data().nullable = root->left()->data().nullable || root->right()->data().nullable;
                std::set_union(root->left()->data().firstpos.begin(), root->left()->data().firstpos.end(),
                               root->right()->data().firstpos.begin(), root->right()->data().firstpos.end(),
                               std::back_inserter(root->data().firstpos) );
                std::set_union(root->left()->data().lastpos.begin(), root->left()->data().lastpos.end(),
                               root->right()->data().lastpos.begin(), root->right()->data().lastpos.end(),
                               std::back_inserter(root->data().lastpos) );
            }
            else if('.' == root->token().value) {
                root->data().nullable = root->left()->data().nullable && root->right()->data().nullable;
                if(root->left()->data().nullable) {
                    std::set_union(root->left()->data().firstpos.begin(), root->left()->data().firstpos.end(),
                                   root->right()->data().firstpos.begin(), root->right()->data().firstpos.end(),
                                   std::back_inserter(root->data().firstpos) );
                }
                else {
                    root->data().firstpos = root->left()->data().firstpos;
                }
                
                if(root->right()->data().nullable) {
                    std::set_union(root->left()->data().lastpos.begin(), root->left()->data().lastpos.end(),
                                   root->right()->data().lastpos.begin(), root->right()->data().lastpos.end(),
                                   std::back_inserter(root->data().lastpos) );
                }
                else {
                    root->data().lastpos = root->right()->data().lastpos;
                }
            }
            else
                throw RegexSyntaxTreeError("Unsupported operator in syntax tree building.");
        }
    }
    
    void evaluate_followpos(SyntaxTreeNodePtr root)
    {
        if(!root || OPERAND == root->token().type)
            return;
        if( '.' == root->token().value) {
            for(auto pos : root->left()->data().lastpos) {
                std::vector<int>& followpos = followPositions.at(pos);
                std::vector<int>& firstpos = root->right()->data().firstpos;
                followpos.insert(followpos.end(), firstpos.begin(), firstpos.end() );
                std::sort(followpos.begin(), followpos.end());
            }
            evaluate_followpos(root->left());
            evaluate_followpos(root->right());
        }
        else if('*' == root->token().value) {
            for(auto pos : root->data().lastpos) {
                std::vector<int>& followpos = followPositions.at(pos);
                followpos.insert(followpos.end(), root->data().firstpos.begin(), root->data().firstpos.end() );
                std::sort(followpos.begin(), followpos.end());
            }
            evaluate_followpos(root->child());
        }
        else {
            assert('|' == root->token().value);
            evaluate_followpos(root->left());
            evaluate_followpos(root->right());
        }
    }
    
    std::vector<DFA::State> construct_transition_table(SyntaxTreeNodePtr root) {
        // data. Always keep the following vectors' length the same.
        std::vector<DFA::State> states;
        std::vector<std::vector<int>> statePositions;
        std::vector<int> marked;
        // init. Attn: keep the length consistent
        //// the first state
        states.push_back(DFA::State());
        statePositions.push_back(root->data().firstpos);
        marked.push_back(0);
        // loop over unmarked state
        auto it = marked.end();
        while( (it=find(marked.begin(), marked.end(), 0)) != marked.end()) {
            size_t current_state_idx = it - marked.begin();
            marked[current_state_idx] = 1;
            for(char c = 0; c < DFA::kCharacterSetSize; ++c) {
                std::vector<int> newStatePositions;
                for( auto pos : statePositions[current_state_idx]) {
                    if(c == this->positionChars[pos]) {
                        newStatePositions.insert(newStatePositions.end(),
                                                 this->followPositions[pos].begin(),
                                                 this->followPositions[pos].end());
                    }
                }
                std::unique(newStatePositions.begin(), newStatePositions.end());
                if(newStatePositions.empty()) // there is no epsilon-transition in DFA
                    continue;
                auto itnew = statePositions.end();
                int next_state_idx = -1;
                if((itnew = std::find(statePositions.begin(), statePositions.end(), newStatePositions)) == statePositions.end()) {
                    // found and add new state. Again, keep the length consistent
                    states.push_back(DFA::State());
                    statePositions.push_back(newStatePositions);
                    marked.push_back(0);
                    next_state_idx = static_cast<int>(statePositions.size()) - 1;
                }
                else
                    next_state_idx = static_cast<int>(itnew - statePositions.begin());
                states[current_state_idx].move[c] = next_state_idx;
            }
        }
        //result
        for(size_t i = 0; i < states.size(); ++i) {
            states[i].positions.swap(statePositions[i]);
            for(auto pos : states[i].positions) {
                if(augmentCharPositions.find(pos) != augmentCharPositions.end()) {
                    states[i].acceptable = true;
                    break;
                }
            }
        }
        return states;
    }
    
    std::vector<int> evaluate_nearest_augpos()
    {
        std::vector<int> augpos(followPositions.size(), -1);
        for(size_t i = 0; i < followPositions.size(); ++i){
            for(auto p : followPositions[i]){
                if(positionChars[p] == augmentChar){
                    augpos[i] = p;
                    break;
                }
            }
        }
        return augpos;
    }
    
    
    /*
     std::unordered_map<int, void (*f)()> install_action_table(const std::vector<void *f()>& actions, const char aug){
     std::unordered_map<int, void (*f)()> action_table();
     auto it = actions.cbegin();
     for(size_t i = 0; i < this->positionChars.size(); ++i){
     if(aug == positionChars[i]){
     action_table[i] == *it++;
     }
     assert(it == actions.end());
     assert(std::find( &this.positionChars[i], this.positionChars.end(), aug) == this.positionChars.end());
     return action_table;
     }
     }*/
    
};



#endif
