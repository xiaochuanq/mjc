//
//  tokens.hpp
//  regex
//
//  Created by Xiaochuan Qin on 12/15/13.
//  Copyright (c) 2013 Xiaochuan Qin. All rights reserved.
//

#ifndef TOKENS_HPP
#define TOKENS_HPP

enum TokenType{
    NA,
//puctuation
    COMMA,
    COLON,
    DOT,
    SEMICOLON,
    LBRACK,
    RBRACK,
    LPAREN,
    RPAREN,
    
// Keywords - control flow
    IF,
    ELSE,
    FOR,
    DO,
    WHILE,
// Keywords - data type and definition
    CLASS,
    PUBLIC,
    PRIVATE,
    INT,
    FLOAT,
    BOOL,
// Operators
    RELOP,
    MATH,
    ASSIGN,

// Numbers
    INTEGER,
    REAL,
    STRING
    
// Identification
    ID
};

enum TokenValue{
    NA,
    
    ASSIGNMENT, // =
    
    EQ, //==
    NE, //!=
    LT, //<
    LE, //<=
    GT, //>
    GE, //>=
    
    PLUS, //+
    MINUS, //-
    MUL, //*
    DIV, // /
    PPLUS, //++
    MMINUS, //--
    LSHIFT, //<<
    RSHIFT, //>>
    
    
    AND, //&&
    OR, //||
    
    BITAND, //&
    BITOR,  //|
    BITXOR, //^
    BITNEG, //~
};

union TokenAttribute{
    int i;
    double r;
    size_t id;
    TokenValue t;
};

struct Token{
    Token(TokenType t, TokenAttribute a) : type(t), attr(a){
        
    }
    TokenType type;
    TokenAttribute attr;
};


std::string token_str(const Token& t){
    return "";
}

#endif
