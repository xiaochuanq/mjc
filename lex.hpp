//
//  lex.hpp
//  regex
//
//  Created by Xiaochuan Qin on 12/15/13.
//  Copyright (c) 2013 Xiaochuan Qin. All rights reserved.
//

#ifndef LEX_HPP
#define LEX_HPP

#include <string>
#include <unordered_map>
#include "tokens.hpp"

struct RegularDefinition{
    RegularDefinition(std::string r) : regex(r){}
    RegularDefinition& operator=(RegularDefinition& other){
        if( *this != other)
            regex = other.regex;
        return *this;
    }
    RegularDefinition(const RegularDefinition& other){
        regex = other.regex;
    }
    std::string regex;
};

std::unordered_map<RegularDefinition,


#endif
