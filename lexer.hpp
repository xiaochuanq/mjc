//
//  lexer.hpp
//  regex
//
//  Created by Xiaochuan Qin on 12/15/13.
//  Copyright (c) 2013 Xiaochuan Qin. All rights reserved.
//

#ifndef regex_lexer_hpp
#define regex_lexer_hpp
#include <fstream>
#include "dfa.hpp"
#include "lexdef.hpp"

class Lexer{
public:
    Lexer(std::ifstream& infs) : stream(infs), currentBuf(0), currentLn(0) {
        memset(buffer[0], stream.eof(), (kBufferSize+1)*sizeof(char));
        memset(buffer[1], stream.eof(), (kBufferSize+1)*sizeof(char));
        buffer[0][kBufferSize] = buffer[1][kBufferSize] = kSentinel;
    }
    
    Token next_token(){
        switch(*lookahead++){
            case kSentinel:
                if(lookahead == &buffer[0][kBufferSize]){
                    load_buffer(1);
                    lookahead = buffer[1];
                }
                else if(lookahead == &buffer[1][kBufferSize]){
                    load_buffer(0);
                    lookahead = buffer[0];
                }
                else{
                    return Token(NA,NA);
                }
                break;
            default:
                
        }
    }
private:
    static const kBufferSize = 4096;
    static char kSentinel = -1;
    std::ifstream stream;
    size_t currentBuf;
    char buffer[2][kBufferSize+1];
    char* lexemeBegin;
    char* lookahead;
    std::string lexeme;
    size_t currentLn;
private:
    void load_buffer(size_t i){
        stream.read(buffer[i], kBufferSize);
        if(!stream){
            stream[i][stream.gcount()] = kSentinel;
        }
    }

    
};

#endif
