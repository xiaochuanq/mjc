/*
 * This file is to provide the most basic support to parse the regular expressions which are
 * likely to be used in a mini compiler's lexical specification. With methods implemented, regular
 * expressions can be tokenized for evalucation, and for syntax tree construction, which in turn
 * can be used to construct DFA directly using tools in dfa.hpp.
 *
 * Since the goal is to demonstrate how to boost a compiler from scratch, I am not implementing
 * a comprehensive regular expression library which confront to POSIX or other standards. So
 * not all syntaxes as this, http://en.wikipedia.org/wiki/Regular_expression, will be supported.
 *
 * Supported operators include:
 * \ (escape)
 * () (parentheses)
 * [abc-z1-9] [^-abc1-9] [^]] []] (brackets)
 * + ? * (closure)
 * - (range)
 * (concatenation)
 * | (alternation)
 *
 * Stuff not supported:
 * . as any single character
 * \n for n as an integer
 * {m, n} as repeating for m to n times
 * \x## as corresponding characters
 * character sets, such as \b, \w, \W, \s, [:graph:] etc.
 * ^ and $ to indicate the begin and end of a string
 *
 * Existing <regex> library from boost or c++11 will not be used - it makes no sense to parse
 * regular expressions using another regular expression library.
 */


#ifndef REGEX_HPP
#define REGEX_HPP
#include <algorithm>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

////////////////////////////////////
class RegexSyntaxError : public std::runtime_error {
public:
	RegexSyntaxError(const std::string& what_arg = "Error!") : std::runtime_error(what_arg){
	}
};

/////////////////////////////////////
enum RegexTokenType
{
	OPERATOR,
	OPERAND,
    FUNCTION
};

struct RegexToken
{
	RegexTokenType type;
	char value;
    
    static const char epsilon = 0;
	static RegexToken make_operator(char op){
		return {OPERATOR, op};
    }

	static RegexToken make_operand(char symbol){
		return {OPERAND, symbol};
	}
    
    static RegexToken make_function(char func){
		return {FUNCTION, func};
	}
};

static const RegexToken kConcatenation = RegexToken::make_operator('.');
static const RegexToken kAlternation = RegexToken::make_operator('|');
static const RegexToken kKleenClousre = RegexToken::make_operator('*');
static const RegexToken kNegation = RegexToken::make_operator('^');
static const RegexToken kRangeOp = RegexToken::make_operator('-');
static const RegexToken kLeftBracket = RegexToken::make_operator('[');
static const RegexToken kRightBracket = RegexToken::make_operator(']');
static const RegexToken kEpsilon = RegexToken::make_operand(0);

bool operator==(const RegexToken& a, const RegexToken& b){
    return a.type == b.type && a.value == b.value;
}

bool operator!=(const RegexToken& a, const RegexToken& b){
    return a.type != b.type || a.value != b.value;
}

//////////////////////////////////////////////////
char escape(char c){
	switch(c){
        case 't':
            return '\t';
        case 'n':
            return '\n';
        case 'r':
            return '\r';
        case 'v':
            return '\v';
        case 'f':
            return '\f';
        default:
            return c;
	}
}

int precedence(char op){
	switch(op){
        case '+': case '?': case '*': case '^':
            return 2;
        case '-': case '.':
            return 1;
        case '|':
            return 0;
        case '(': case ')': case '[': case ']':
            return -1;
        default:
            throw RegexSyntaxError("Invalid Operator");
	}
}

int num_of_operands(char op){
    switch(op){
		case '+': case '?': case '*':
			return 1;
		case '-': case '.': case '|':
			return 2;
		default:
			return 0;
    }
}

bool is_metacharacter(char c){
	// The usual metacharacters are {}[]()^$.|*+? and \\(backslash)
	//  Not supporting .{}$ and ^ as string beginning.
	return '|' == c || '*' == c || '?' == c || '+' == c || '(' == c ||
			')'== c || '[' == c || ']' == c || '^' == c || '\\' == c;
}

/////////////////////////////////////////////////

/*
 * Convert regular expression to a vector of tokens
 */
std::vector<RegexToken> tokenize(const std::string& reg )
{
	std::vector<RegexToken> tokens;
	bool inBrackets = false, escaping = false;
	for (size_t i = 0; i < reg.size(); ++i) {
		char current = reg[i];
		if (inBrackets) { // In brackets
			switch (current) {
			case '\\': // In this case, '\\' does not escape everything
				if( i+1 >= reg.size() )
					throw RegexSyntaxError("Regular expression not terminated correctly. Missing ].");
                if(reg[i+1] == ']'){
                    tokens.push_back(RegexToken::make_operand('\\'));
                }
				else if(escape(reg[i+1]) != reg[i+1]){ // next character is escapble
                    ++i; // go to next character
					tokens.push_back(RegexToken::make_operand(escape(reg[i])));
                }
				else // treat '\\' as a non-metacharacter
					tokens.push_back(RegexToken::make_operand(current));
				break;
			case '^':
				if (reg[i - 1] == '[')
					tokens.push_back(RegexToken::make_operator(current));
				else {
					tokens.push_back(RegexToken::make_operand(current));
				}
				break;
			case '-':
				if (reg[i-1] == '[' || (reg[i - 1] == '^' && reg[i - 2] == '[')
						|| (i + 1 < reg.size() && reg[i + 1] == ']')) {
					tokens.push_back(RegexToken::make_operand(current));
				} else {
					tokens.push_back(RegexToken::make_operator(current));
				}
				break;
			case ']':
				if (reg[i - 1] == '[' ||
				    (reg[i - 1] == '^' && reg[i - 2] == '[')) {
					tokens.push_back(RegexToken::make_operand(current));
				} else {
					inBrackets = false;
					tokens.push_back(RegexToken::make_operator(current));
				}
				break;
			default:
				tokens.push_back(RegexToken::make_operand(current));
			}
		}
		else { // Not in brackets
			if (!escaping) {
				if('\\' == current){
					escaping = true;
				}
                else if('^' == current){
                    /*if(i == 0)
                        tokens.push_back(RegexToken::make_operator(current));
                    else
                     */
                    // The leading ^ to indicate string begin is not supported
                    tokens.push_back(RegexToken::make_operand(current));
                }
				else if( is_metacharacter(current)){
					tokens.push_back(RegexToken::make_operator(current));
					if( '[' == current){
						inBrackets = true;
                    }
				}
				else // literal
					tokens.push_back(RegexToken::make_operand(current));
			}
			else{ // escaping, is after '\\'
				escaping = false;
				tokens.push_back(RegexToken::make_operand(escape(current)));
			}
		}
	}
    
	if( inBrackets)
		throw RegexSyntaxError("Regular expression not terminated correctly. Missing ].");
	if(escaping)
		throw RegexSyntaxError("Dangling escape opeartor. No characters after \\.");

	return tokens;
}


/*
 * Expand vector of regular expressions tokens by inserting the concatnations and alternations
 */
std::vector<RegexToken> make_verbose(const std::vector<RegexToken>& tokens)
{
	std::vector<RegexToken> result;
    bool inBrackets = false;
    for(size_t i = 0; i < tokens.size(); ++i){
        const RegexToken& current = tokens[i];
        result.push_back(current);
        if(OPERAND == current.type){
            if(i+1 < tokens.size()){// there is next token
                const RegexToken& next = tokens[i+1];
                if( OPERAND == next.type){
                    if(inBrackets)
                        result.push_back(kAlternation);
                    else
                        result.push_back(kConcatenation);
                }
                else if(OPERATOR == next.type){
                    if('(' == next.value || '[' == next.value)
                        result.push_back(kConcatenation);
                }
            }
        }
        else if(OPERATOR == current.type){
            if('[' == current.value)
                inBrackets = true;
            else if(']' == current.value){
                inBrackets = false;
            }
            if(i+1 < tokens.size()){
                const RegexToken& next = tokens[i+1];
                if( OPERAND == next.type){ //current operator followed by an operand, as long as it is not ( or [ or |
                    if( '*' == current.value ||
                        '?' == current.value ||
                        '+' == current.value ||
                        ')' == current.value ||
                        ']' == current.value)
                        result.push_back(kConcatenation);
                }
                else if( OPERATOR == next.type){ // both current and next are operators
                    if( '(' == next.value || '[' == next.value ){
                        /*if( '(' != current.value || '|' != current.value)
                            result.push_back(kConcatenation);*/
                        if('*' == current.value ||
                           '?' == current.value ||
                           '+' == current.value ||
                           ')' == current.value ||
                           ']' == current.value){
                            result.push_back(kConcatenation);
                        }
                    }
                }
                
            }
        }
    }
    return result;
}

/*
 * Given the vector of tokens inside a [ ] pair, evaluate it to a vector of all acceptable alphabet.
 */

std::vector<RegexToken> evaluate_character_set(std::vector<RegexToken>::const_iterator tokens_begin,
                                               std::vector<RegexToken>::const_iterator tokens_end,
                                               const std::set<char>& allchar)
{
    std::vector<RegexToken> result;
    std::vector<char> chars;
    bool neg = false;
    std::vector<RegexToken>::const_iterator next = tokens_begin, current = tokens_begin;
    if(current != tokens_end){
        if( *current == kNegation){
            neg = true;
            ++current;
        }
        next = current;
        while(current != tokens_end){
            ++next;
            if(next == tokens_end || next->type == OPERAND){
                // current is either the last token or the following token is an operator
                chars.push_back(current->value);
            }
            else{ // next is an operator. '-' is the only legal one in [].
                //if( '-' == next->value ){ // e.g. a-zA-Z  it is a, next is -
                if(*next == kRangeOp){
                    ++next; // now move next to z
                    if(next == tokens_end)
                        throw RegexSyntaxError("Operand expecated after operator '-' in [].");
                    if(next->value < current->value )
                        throw RegexSyntaxError(
                                               "Operands of '-' in []");
                    for(char c = current->value; c <= next->value; ++c){
                        chars.push_back(c);
                    }
                    ++next; // in the example, move it to A
                }
                else
                    throw RegexSyntaxError("Only '-' is accepted as the operator in [].");
            }
            current = next;
        }
    }
    if(neg){
        std::set<char> charset(allchar); // duplicate only when needed
        for(auto c : chars)
            charset.erase(c);
        for(auto c : charset)
            result.push_back(RegexToken::make_operand(c));
    }
    else{
        for(auto c : chars)
            result.push_back(RegexToken::make_operand(c));
    }
    return result;
}

/*
 * Replace character set, i.e. [^ a-bXYZ- ] with [ all acceptable characters ]
 */
std::vector<RegexToken> expand_character_set(const std::vector<RegexToken>& tokens, const std::set<char>& allchar)
{
    std::vector<RegexToken> result;
    for(auto current = tokens.cbegin(); current != tokens.cend(); ++current ){
        result.push_back(*current);
        if( *current == kLeftBracket){
            auto right = find(current, tokens.cend(), kRightBracket);
            if( right == tokens.cend())
                throw RegexSyntaxError("Unbalanced []");
            std::vector<RegexToken> charset = evaluate_character_set(++current, right, allchar);
            result.insert(result.end(), charset.begin(), charset.end());
            result.push_back(*right);
            current = right;
        }
    }
    return result;
}

/*
 * Do all the evaluations of [], and insert concatenation and alterantions into the token vector
 */
std::vector<RegexToken> preoprocess(const std::vector<RegexToken>& tokens, const std::set<char>& allchar)
{
    return make_verbose(expand_character_set(tokens, allchar));
}


/*
 * Construct the Reverse Polish Notation of a regular expression; the input must be a vector of preprocessed tokens
 */
std::vector<RegexToken> make_rpn(const std::vector<RegexToken>& tokens)
{
	std::vector<RegexToken> stack, rpn;
    for(auto t : tokens){
        if(OPERAND == t.type){
            rpn.push_back(t);
        }
        else if(OPERATOR == t.type){
            if( '(' == t.value || '[' == t.value)
                stack.push_back(t);
            else if( ')' == t.value ){
                while( !stack.empty() && '(' != stack.back().value){
                    rpn.push_back(stack.back());
                    stack.pop_back();
                }
                if(stack.empty()){
                    throw RegexSyntaxError("Unbalanced parantheses.");
                }
                else
                    stack.pop_back();//pop '('
            }
            else if( ']' == t.value){
                while( !stack.empty() && '[' != stack.back().value){
                    rpn.push_back(stack.back());
                    stack.pop_back();
                }
                if(stack.empty()){
                    throw RegexSyntaxError("Unbalanced brackets.");
                }
                else
                    stack.pop_back();//pop '['
            }
            else{
                while( !stack.empty() && precedence(t.value) <= precedence(stack.back().value) ){
                    rpn.push_back(stack.back());
                    stack.pop_back();
                }
                stack.push_back(t);
            }
        }
        else{
            throw RegexSyntaxError("Unsupported token type.");
        }
    }
    while(!stack.empty()){
        if(')' == stack.back().value || '(' == stack.back().value )
            throw RegexSyntaxError("Unbalanced parantheses.");
        else if( ']' == stack.back().value || '[' == stack.back().value)
            throw RegexSyntaxError("Unbalanced brackets.");
        else{
            rpn.push_back(stack.back());
            stack.pop_back();
        }
            
    }
    return rpn;
}

#endif //DFA_HPP
