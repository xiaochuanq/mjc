//
//  regex_syntax_tree.h
//  regex
//
//  Created by Xiaochuan Qin on 12/16/13.
//  Copyright (c) 2013 Xiaochuan Qin. All rights reserved.
//

#ifndef REGEX_SYNTAX_TREE_H
#define REGEX_SYNTAX_TREE_H

#include <cassert>
#include <memory>
#include <sstream>

#include "regex.hpp"

////////////////////////////
class RegexSyntaxTreeError : public std::runtime_error {
public:
	RegexSyntaxTreeError(const std::string& what_arg = "Error!") : std::runtime_error(what_arg){
	}
};

////////////////////////////

template<typename DataType>
class RegexSyntaxTreeNode{
public:
    RegexSyntaxTreeNode(RegexToken t) : token_(t), data_(nullptr){
        ptrs_[0] = ptrs_[1] = nullptr;
    };
    
    RegexSyntaxTreeNode(const RegexSyntaxTreeNode& r) : token_(r.token_), data_(nullptr){
        ptrs_[0] = ptrs_[1] = nullptr;
    };
    
    ~RegexSyntaxTreeNode(){ clear_data(); }
    
    RegexToken token() const { return token_;}
    
    const DataType& data() const {
        if(!data_)
            throw RegexSyntaxTreeError("Regex syntax tree node data haven't been initialized.");
        return *data_;
    }
    DataType& data(){
        if(!data_)
            init_data();
        if(!data_)
            throw RegexSyntaxTreeError("Regex syntax tree node data haven't been initialized.");
        return *data_;
    }
    
    void init_data(){ data_.reset(new DataType()); }
    void clear_data(){ data_.release();}
    
    const RegexSyntaxTreeNode<DataType>* left() const { return ptrs_[0];}
    const RegexSyntaxTreeNode<DataType>* right() const { return ptrs_[1];}
    const RegexSyntaxTreeNode<DataType>* child() const { return ptrs_[0];}
    
    RegexSyntaxTreeNode<DataType>* left() { return ptrs_[0]; }
    RegexSyntaxTreeNode<DataType>* right() { return ptrs_[1]; }
    RegexSyntaxTreeNode<DataType>* child() { return ptrs_[0]; }
    
    void left(RegexSyntaxTreeNode<DataType>* n) { ptrs_[0] = n; }
    void right(RegexSyntaxTreeNode<DataType>* n) { ptrs_[1] = n; }
    void child(RegexSyntaxTreeNode<DataType>* n) { ptrs_[0] = n; }
    
    bool is_leaf(){ return OPERAND == token_.type; }
    int num_of_operands(){ return OPERATOR == token_.type ? ::num_of_operands(token_.value) : 0; }
private:
    RegexToken token_;
    std::unique_ptr<DataType> data_;
    RegexSyntaxTreeNode<DataType>* ptrs_[2];
};


////////// Helper Functions
template<typename DataType>
RegexSyntaxTreeNode<DataType>* deep_copy(const RegexSyntaxTreeNode<DataType>* root){
    if(!root)
        return nullptr;
    RegexSyntaxTreeNode<DataType>* clone = new RegexSyntaxTreeNode<DataType>(*root);
    clone->left(deep_copy(root->left()));
    clone->right(deep_copy(root->right()));
    return clone;
}

template<typename DataType>
RegexSyntaxTreeNode<DataType>* make_node(RegexToken t, RegexSyntaxTreeNode<DataType>* c)
{ // Generally used to construct Kleen closure * node.
    assert(OPERATOR == t.type && num_of_operands(t.value) == 1);
    RegexSyntaxTreeNode<DataType>* node = new RegexSyntaxTreeNode<DataType>(t);
    node->child(c);
    return node;
}

template<typename DataType>
RegexSyntaxTreeNode<DataType>* make_node(RegexToken t, RegexSyntaxTreeNode<DataType>* l, RegexSyntaxTreeNode<DataType>* r)
{ // Used to construct alternation | or concatenation . node
    assert(OPERATOR == t.type && num_of_operands(t.value) == 2);
    RegexSyntaxTreeNode<DataType>* node = new RegexSyntaxTreeNode<DataType>(t);
    node->left(l);
    node->right(r);
    return node;
}

template<typename DataType>
RegexSyntaxTreeNode<DataType>* make_node(RegexToken t)
{ // used to construct leaf node
    assert(OPERAND == t.type);
    RegexSyntaxTreeNode<DataType>* node = new RegexSyntaxTreeNode<DataType>(t);
    return node;
}

///////////// Tree Operations

template<typename DataType>
RegexSyntaxTreeNode<DataType>* construct_regex_syntax_tree(const std::vector<RegexToken>& rpn){
    RegexSyntaxTreeNode<DataType>* root = nullptr;
    if(!rpn.empty()){
        std::vector< RegexSyntaxTreeNode<DataType>* > stack;
        for(auto token : rpn){
            if(OPERAND == token.type){
                stack.push_back(make_node<DataType>(token));
            }
            else if(OPERATOR == token.type){
                assert( stack.size() >= num_of_operands(token.value) );
                RegexSyntaxTreeNode<DataType>* tmp1, *tmp2;
                switch(token.value){
                    case '+':
                        tmp1 = stack.back();
                        stack.pop_back();
                        stack.push_back(make_node<DataType>(kConcatenation, tmp1, make_node<DataType>(kKleenClousre, deep_copy(tmp1)) ));
                        break;
                    case '?':
                        tmp1 = stack.back();
                        stack.pop_back();
                        stack.push_back(make_node<DataType>(kAlternation, make_node<DataType>(kEpsilon), tmp1));
                        break;
                    case '*':
                        tmp1 = stack.back();
                        stack.pop_back();
                        stack.push_back(make_node<DataType>(kKleenClousre, tmp1));
                        break;
                    case '|': case '.':
                        tmp2 = stack.back(); stack.pop_back();
                        tmp1 = stack.back(); stack.pop_back();
                        stack.push_back(make_node(token, tmp1, tmp2));
                        break;
                    default:
                        throw RegexSyntaxTreeError("Unsupported operator in syntax tree building.");
                }
            }
        }
        root = stack.back();
        stack.pop_back();
    }
    return root;
}

template<typename DataType>
void destroy_regex_syntax_tree(RegexSyntaxTreeNode<DataType>* root){
    if(!root)
        return;
    delete_tree(root->left());
    delete_tree(root->right());
    delete root;
}

template<typename DataType>
void clear_regex_syntax_tree_data(RegexSyntaxTreeNode<DataType>* root){
    if(!root)
        return;
    root->clear_data();
    clear_regex_syntax_tree_data(root->left());
    clear_regex_syntax_tree_data(root->right());
}
                                  

template<typename DataType>
std::string make_expression(const RegexSyntaxTreeNode<DataType>* root){
    std::string s = "";
    if(root){
        if(OPERAND == root->token().type)
            s.append(1,root->token().value);
        else{
            if('*' == root->token().value){
                s.append("(");
                s.append( make_expression(root->child()));
                s.append(")");
                s.append("*");
            }
            else if('|' == root->token().value){
                s.append("(");
                s.append(make_expression(root->left()));
                s.append("|");
                s.append(make_expression(root->right()));
                s.append(")");
            }
            else{
                s.append(make_expression(root->left()));
                s.append(make_expression(root->right()));
            }
        }
    }
    return s;
}


#endif
